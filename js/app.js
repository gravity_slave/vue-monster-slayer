new Vue({
   el: '#app',
    data: {
        playerHealth: 100,
        monsterHealth: 100,
        gameIsRunning: false,
        turns: []
    },
    methods: {
        startGame: function () {
            this.gameIsRunning = true;
            this.playerHealth = 100;
            this.monsterHealth = 100;
            this.turns = [];
        },
        attack: function () {

          var damage =  this.calculateDamage(3, 10);
          this.monsterHealth -= damage;
            this.turns.unshift(({
                isPlayer: true,
                text: 'Player hits monster for ' + damage
            }));
             if (this.checkWin()) {
                 return;
             }
            this.monsterAttacks();

        },
        specialAttack: function () {
            var damage = this.calculateDamage(10, 20);
            this.monsterHealth -= damage;
            this.turns.unshift(({
                isPlayer: true,
                text: 'Player hits monster with special attack for ' + damage
            }));
            if (this.checkWin()) {
                return;
            }
            this.monsterAttacks();
        },
        heal: function () {
            var heal;
            if (this.playerHealth <= 90) {
                heal = 10;
                this.playerHealth += heal;
            } else {
                heal = 100 - this.playerHealth;
                this.playerHealth += (100 - heal);
            }
            this.turns.unshift(({
                isPlayer: true,
                text: 'Player heals himself for ' + heal
            }));
            this.monsterAttacks();
        },
        giveUp: function () {
            this.gameIsRunning = false;
        },
        calculateDamage: function (min, max) {
            return Math.max(Math.floor(Math.random() * max + 1), min);

        },
        checkWin: function () {
            if (this.monsterHealth <= 0) {
                if (confirm('You Won! New Game?')) {
                    this.startGame();
                } else {
                    this.gameIsRunning = false;
                }
                return true;
            } else if (this.playerHealth <= 0) {
                if (confirm('You Lost! New Game?')) {
                    this.startGame();
                } else {
                    this.gameIsRunning = false;

                }
                return true;
            }

            return false;
        },
        monsterAttacks: function () {
           var damage = this.calculateDamage(5, 12);
           this.playerHealth -= damage;
            this.turns.unshift(({
                isPlayer: false,
                text: 'Monster hits player for ' + damage

            }));
            this.checkWin();
        }

    }
});